<?php session_start();
include ('functions.inc.php');?>
<html>
<head>
    <title><?=$title?></title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="assets/css/general.css">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/banner.css">
    <link rel="stylesheet" href="assets/css/index.css">
    <link rel="stylesheet" href="assets/css/nav.css">
    <link rel="stylesheet" href="assets/css/list_blog.css">
    <link rel="stylesheet" href="assets/css/list_post.css">
    <link rel="stylesheet" href="assets/css/topbar.css">
    <link rel="stylesheet" href="assets/css/newpost.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <script src="assets/js/jquery-1.9.1.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/tinymce/tinymce.min.js"></script>
  
    
    <script>

   	$('[data-toggle="tooltip"]').tooltip();
    tinymce.init({
        selector: '#content_npost',
        menubar: false,
        plugins: [
                  'advlist autolink lists link image charmap print preview anchor',
                  'searchreplace visualblocks code fullscreen',
                  'insertdatetime media table contextmenu paste code',
                  'textcolor',
                ],
       toolbar: 'undo redo |insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
      });
    
    </script>
</head>
<body>
          <?php 
          if(!isset( $_SESSION['username']))
          {
            header("Location:/blog");
          }
          else{
          	include ('navbar.php');
          }
            ?> 
	<div class="wrapper col-md-10 col-md-offset-2">
	    <div class="topbar">
	    	<a href="logout.php"><i class="fa fa-user-times" aria-hidden="true"></i></a>
	    	<?=$_SESSION['username']?>
	    </div>