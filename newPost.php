<?php
$title="Posts";
include ('include/header.php');
if(isset($_GET['id']))
{
	$id_blog=$_GET['id'];
	?>
<div class="npost-form">
	<h1>Nouveau post</h1>
	<form method="post" >
	    <input type="hidden" name="user_npost" value="<?=$_SESSION['username'];?>">
	    <input type="hidden" name="date_npost" value="<?=date("Y-m-d H:i:s");?>">
	    <input type="hidden" name="id_blog" value="<?=$id_blog?>">
	    <div class="align-center">
		    <label for="title_npost">Titre</label>
		    <input type="text" name="title_npost" id="title_npost">
		</div>
		<div>
		    <label for="content_npost">Contenu</label>
		    <textarea id="content_npost" name="content_npost"></textarea>
	    </div>
	    <div class="align-right">
	    	<input type="submit" value="Envoyer" name="submit_npost">
	    </div>
	</form>
</div>
<?php
}
if(isset($_POST["submit_npost"]) && isset($_POST["date_npost"])&& isset($_POST["title_npost"])&& isset($_POST["content_npost"]))
{
    $user=$_POST["user_npost"];
    $date=$_POST["date_npost"];
    $title=$_POST["title_npost"];
    $content=$_POST["content_npost"];
    $id_blog=$_POST['id_blog'];
    addPost($user,$title,$date,$content,$id_blog);
}
?>