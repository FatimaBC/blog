<?php session_start();?>
<html>
<head>
    <title>Accueil</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="assets/css/general.css">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/banner.css">
    <link rel="stylesheet" href="assets/css/index.css">
    <link rel="stylesheet" href="assets/css/nav.css">
    <link rel="stylesheet" href="assets/css/list_blog.css">
    <link rel="stylesheet" href="assets/css/list_post.css">
    <link rel="stylesheet" href="assets/css/topbar.css">
    <link rel="stylesheet" href="assets/css/newpost.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <script src="assets/js/jquery-1.9.1.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/tinymce/tinymce.min.js"></script>
</head>
<body>
<?php 
include ('functions.inc.php');
$title="Accueil";
if (isset($_SESSION['username']) ) //if user authenticated
{
	include ('include/navbar.php');
    ?>
    <div class="wrapper col-md-10 col-md-offset-2">
	    <div class="topbar">
	    	<a href="logout.php"><i class="fa fa-user-times" aria-hidden="true"></i></a>
	    	<?=$_SESSION['username']?>
	    </div>
	<?php include ('users.php');?>
<?php 
}
else
{
?>
<div class="banner">
    <div class="banner-title">
        <div class="banner-logo"></div>
    </div>
</div>
    <div class="image-banner align-center">
        <h1>Créez vos blogs, partagez vos humeurs</h1>
    </div>
    <div class="index-wrapper-connexion">
        <h1 class="index-title align-center"><img src="assets/img/ordinnateur.png">Connexion</h1>
        <div class="index-form">
            <form method="post">
                <div class="row">
                    <label for="connect_login" class="col-md-4">Login</label>
                    <input type="text" id="connect_login" name="connect_login" class="col-md-6">
                </div>
                <div class="row">
                    <label for="connect_pswd" class="col-md-4">Mot de passe</label>
                    <input type="password" id="connect_pswd" name="connect_pswd" class="col-md-6">
                </div>
                <div class="align-center index-form-submit">
                    <input type="submit" id="connect_submit" name="connect_submit" value="Connexion">
                </div>
            </form>
        </div>
    </div>
    <div class="index-wrapper-info align-center">
        <div class="index-info-text">
            <h1> Pas encore inscrit? </h1>
            <p>Envie de partager tes expériences avec la e-communauté ?</p>
            <h2><img class="index-info-img" src="assets/img/perso.png">Rejoins-nous !</h2>
        </div>
    </div>
    <div class="index-wrapper-inscription">
        <h1 class="hidden">Inscription</h1>
        <div class="index-form">
            <form method="post">
                <div class="row">
                    <label for="sign_login" class="col-md-4">Login</label>
                    <input type="text" id="sign_login" name="sign_login" class="col-md-6">
                </div>
                <div class="row">
                    <label for="sign_pswd" class="col-md-4">Mot de passe</label>
                    <input type="password" id="sign_pswd" name="sign_pswd" class="col-md-6">
                </div>
                <div class="row">
                    <label for="sign_pswd_c" class="col-md-4">Confirmer mot de passe</label>
                    <input type="password" id="sign_pswd_c" name="sign_pswd_c" class="col-md-6">
                </div>
                <div class="align-center index-form-submit">
                    <input type="submit" id="sign_submit" name="sign_submit" value="S'inscrire">
                </div>
            </form>
    <?php 
    //sign
    if(isset($_POST['sign_submit']) && isset($_POST['sign_login']) && isset($_POST['sign_pswd']) && isset($_POST['sign_pswd_c']))
    {
        if($_POST['sign_pswd']==$_POST['sign_pswd_c'])
        {
            if(getUser($_POST['sign_login'])!=null)
            {
                $erreur='Erreur Deja prit';
            }
            else{
                addUser($_POST['sign_login'],$_POST['sign_pswd']);
                echo 'Utilisateur enregistré!';
            }
        }
        else
        {
            $erreur="Erreur: Vous avez entré des mots de passes différents.";
        }
    }
        ?>    <div class="erreur-inscription" id="error_sign">
                <?php
                if(isset($erreur))
                {
                    echo $erreur;
                }
                ?>
            </div>
        </div>
    </div>
<?php //login
    if(isset($_POST['connect_submit']) && isset($_POST['connect_login']) && isset($_POST['connect_pswd']))
    {
        verifyUser($_POST['connect_login'], $_POST['connect_pswd']);
    }
}  include ('include/footer.php'); ?>