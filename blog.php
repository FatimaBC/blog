<?php
$title="Blogs";
include ('include/header.php');?>
<?php 
if(isset($_GET['id']))
{
	$id=$_GET['id'];
	?>
	<div class="list-blog-wrap">
	<div class="row">
	<?php 
	if($id == $_SESSION['username'])
	{
	?>
	<h1 class="list-blog-title align-center col-md-10 ">Vos blogs</h1>
	<a href="newBlog.php" class="list-ajouter-blog col-md-1" ><img src="assets/img/plus.png"></a>
	<?php }
	else{
		?><h1 class="list-blog-title align-center col-md-10 ">Les blogs de <?=$id?> </h1>
	<?php }
	?>
	</div>
	<div class="list-blog-content ">
	<p>Vous avez <?=getNbBlogsById($id)?> blogs.</p>
	   		<ul>
	    <?php
	    foreach($list=getAllBlogsById($id) as $blog)
	    {
	        ?>	        
		        <li>
		        	<a href="list.php?id=<?=$blog[0]?>"><?=$blog[1]?></a>
		        	<p>Créé le <?=$blog[3]?></p>
		        </li>	    
		<?php
		}?>
			</ul>
		</div>	
	</div>
	<?php 
}?>


<?php include ('include/footer.php');?>