# README #

#  Pages
* Connexion/Inscription
* Page de recherche de contacts (membres)
* Mes blogs
* Créer un nouveau blog
* Mon blog (Tous les posts du blog)
* Nouveau post
* Page d'un post en particulier (avec possibilité de répondre au post et de voir toutes les réponses)

# Fonctionnalites abouties #
* Connexion/Inscription
* Ajout blog
* Ajout post
* Voir d'autres blogs d'autres personnes
* Repondre a des posts

# Fonctionnalites non abouties  #
* Style a ameliorer 
* Pas de choix d'affichage des reponses