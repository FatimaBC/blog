<?php
$title="Posts";
include ('include/header.php');
if(isset($_GET['id']))
{
    $id_blog=$_GET['id'];
    $id_user=getUserByBlogId($id_blog);?>
   <div class="list-post-wrap">
	   <div class="row">
	   		<h1 class="list-blog-title list-post-title align-center col-md-10"><?= getBlogTitleById($id_blog)?></h1>
	   		<?php if($_SESSION['username']==$id_user){ ?>
		   	<a href="newPost.php?id=<?=$id_blog?>" class="list-ajouter-blog col-md-1" data-toggle="tooltip" data-placement="top" title="Tooltip on top">
	   			<img src="assets/img/plus.png">
	   		</a>
		    <?php }?>	   		
	   	</div>
	    <?php
	    foreach($list=getAllPostsByIds($id_user,$id_blog) as $post)
	    {
	    ?>
	    <div class="post-blog-wrapper row" >
	   		<div class="post-container col-md-11">
	            <div class="post-blog-title align-center">
	                <h1><?=$post[2]?></h1>
	            </div>
	            <div class="post-blog-date align-center">
	               Le <?=$post[4]?> par <?=$post[1]?>
	            </div>
	            <div class="post-blog-content">
	            	<?=$post[3]?>
	            </div>
	            <div class="align-right">
	            	 <a href="post.php?id=<?=$post[0]?>"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Lire la suite</a>
	            </div>
	         </div>
	    </div>
	    <?php }?>
	 </div>
  <?php  
}
?>