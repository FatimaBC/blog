<?php
$title="Blog";
include ('include/header.php');
if(isset($_GET['id']))
{
    $id=$_GET['id'];
    $post=getPostById($id);
    if($post!=null)
    {
        ?>
        <div class="col-md-11">
        <!-- Post -->
        <div class="post-blog-wrapper">
           <div class="align-right">
	            Nb de réponses: <?=getNbAnswers($post[0])?>.<br/>
	            Le <?=$post[4]?>
	            Par  <?=$post[1]?>
            </div> 
            <div class="post-blog-title align-center">
                <h1><?=$post[2]?></h1>
            </div>
            <div class="post-blog-content">
                <?=$post[3]?>
            </div>
 			<div>
                <form method="post">
                    <input type="hidden" name="id_post" value=<?=$id?>>
                    <input type="hidden" name="id_prec" value="null">
                    <input type="submit" name="answer_post" value="Répondre">
                 </form>
            </div>        
        </div>
      <!-- Answers -->
            <?php
            $list=getAnswersByPost($post[0]);
            foreach($list as $answer)
            {
                ?>
                <div class="col-md-10 col-md-offset-<?=$answer[6]?>">
	                <div class="answer">
	                    <div class="profil">
	                        <?=$answer[1]?>
	                        Le <?=$answer[5]?>
	                    </div>
	                    <div class="title">
	                        <h1><?=$answer[3]?></h1>
	                    </div>
	                    <div class="content">
	                        <?=$answer[4]?>
	                    </div>
	                    <div>
			                <form method="post">
			                    <input type="hidden" name="id_post" value=<?=$id?>>
			                    <input type="hidden" name="level" value="<?=$answer[6]?>">
			                    <input type="submit" name="answer_answer" value="Répondre">
			                 </form>
			            </div> 
	                </div>
                </div>
                <?php
            }
            ?>      
        <div class="clearfix"></div>
      <?php if(isset($_POST['answer_post']))
      {
      
    ?>
        <!-- Comment Zone answer post-->
        <form method="post">
        	<input type="hidden" name="level" value="0">
            <input type="hidden" name="comment_date" value="<?=date('Y-m-d H:i:s')?>">
            <input type="hidden" name="comment_post_id" value="<?=$post[0]?>">
            <input type="hidden" name="comment_user" value="<?=$_SESSION['username']?>">
            <label for="comment_titre">Titre</label>
            <input type="text" name="comment_titre" id="comment_titre" value="<?=$post[2]?>">
            <textarea id="comment" name="comment"></textarea>
            <input type="submit" name="submit_comment" value="Envoyer">
        </form>
        </div>
    <?php
      }
       if(isset($_POST['answer_answer']))
      {
      
      	?>
              <!-- Comment Zone answer answer-->
              <form method="post">
              	  <input type="hidden" name="level" value="<?=$_POST['level']+1 ?> ">
                  <input type="hidden" name="comment_date" value="<?=date('Y-m-d H:i:s')?>">
                  <input type="hidden" name="comment_post_id" value="<?=$post[0]?>">
                  <input type="hidden" name="comment_user" value="<?=$_SESSION['username']?>">
                  <label for="comment_titre">Titre</label>
                  <input type="text" name="comment_titre" id="comment_titre" value="<?=$post[2]?>">
                  <textarea id="comment" name="comment"></textarea>
                  <input type="submit" name="submit_comment" value="Envoyer">
              </form>
          <?php
            }
    }
    else{
        echo 'erreur durl';
    }
}
//add comment
if (isset($_POST['comment_date']) && isset($_POST['comment_user']) && isset($_POST['comment_post_id']) && isset($_POST['submit_comment']) && isset($_POST['comment_titre']) && isset($_POST['level']) && isset($_POST['comment']))
{
    addComment($_POST['comment_post_id'],$_POST['comment_user'],$_POST['comment_titre'],$_POST['comment'],$_POST['comment_date'], $_POST['level']);
}
?>
