<h1 class="align-center">Membres</h1>
<div class="row">
	<div class="users-list align-center" id="users-list">
		<div class="align-right">
			<input class="search" placeholder="Search" />
			<button class="sort" data-sort="name">
		    	Rechercher
		  	</button>
		  </div>
		<ul class="row list" >
	<?php
	//Liste des users
	 $users=getUsersExceptId($_SESSION['username']);
	 foreach($users as $user)
	 {
	 	 ?><li class="align-center col-md-1">
			 <a href="blog.php?id=<?=$user?>">	
			 	<i class="fa fa-user-circle" aria-hidden="true"></i>
			 	<span class="name"><?=$user?></span>
			 </a>
	 		</li>
	<?php 	
	 }?>
	 	</ul>
	 	<ul class="pagination"></ul>		
	</div>
</div>
<script src="//cdnjs.cloudflare.com/ajax/libs/list.js/1.5.0/list.min.js"></script>
<script>
	var options = {
		    valueNames: ['name']
		};
	var hackerList = new List('users-list', options);
</script>