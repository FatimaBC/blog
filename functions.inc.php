<?php
function getUsersExceptId($id)
{
	$users=array();
	$row=1;
	if(($handle=fopen('csv/user.csv.txt','r')) !== FALSE)
	{
		while(($data=fgetcsv($handle,1000,';')) !==FALSE)
		{
			if($row == 1){ $row++; continue; }
			if($data[0]!=$id)
			{
				array_push($users,$data[0]);
			}
			$row++;
		}
		fclose($handle);
	}
	return $users;
}
function getAnswersByAnswer($id)
{
	$answers=array();
	$row=1;
	if(($handle=fopen('csv/answer.csv.txt','r')) !== FALSE)
	{
		while(($data=fgetcsv($handle,1000,';')) !==FALSE)
		{
			if($data[6]==$id)
			{
				array_push($answers, $data);
				break;
			}
			$row++;
		}
		fclose($handle);
	}
	return $answers;
}
function getUserByBlogId($id)
{
	$user=null;
	$row=1;
	if(($handle=fopen('csv/blog.csv.txt','r')) !== FALSE)
	{
		while(($data=fgetcsv($handle,1000,';')) !==FALSE)
		{
			if($data[0]==$id)
			{
				$user=$data[2];
				break;
			}
			$row++;
		}
		fclose($handle);
	}
	return $user;
}
function getAllBlogs()
{
	$list=array();
	$row=1;
	if(($handle=fopen('csv/blog.csv.txt','r')) !== FALSE)
	{
		while(($data=fgetcsv($handle,1000,';')) !==FALSE)
		{
			if($row == 1){ $row++; continue; }
			array_push($list,$data);
			$row++;
		}
		fclose($handle);
	}
	return $list;
}
function getBlogTitleById($id)
{
	$row=1;
	$title="";
	if(($handle=fopen('csv/blog.csv.txt','r')) !== FALSE)
	{
		while(($data=fgetcsv($handle,1000,';')) !==FALSE)
		{
			if($id==$data[0]) 
			{
				$title=$data[1];
			}
			$row++;
		}
		fclose($handle);
	}
	return $title;
}
function getAnswersByPost($id)
{
    $row=1;
    $list=array();
    if(($handle=fopen('csv/answer.csv.txt','r')) !== FALSE)
    {
        while(($data=fgetcsv($handle,1000,';')) !==FALSE)
        {
            if($id==$data[0]) {
                array_push($list, $data);
            }
            $row++;
        }
        fclose($handle);
    }
    return $list;
}
function addComment($post, $user,$title, $content,$date,$level)
{
    $answer=getNbAnswers($post)+1;
    if(($handle=fopen('csv/answer.csv.txt','a')) !== FALSE)
    {
        $ligne=array($post,$user, $answer, $title, $content, $date,$level);
        fputcsv($handle, $ligne, ';');
        echo 'Ajout�';
    }
    fclose($handle);
}
function getNbAnswers($id)
{
    $row=0;
    if(($handle=fopen('csv/answer.csv.txt','r')) !== FALSE)
    {
        while(($data=fgetcsv($handle,1000,';')) !==FALSE)
        {
            if($data[0]==$id)
            {
                $row++;
            }
        }
        fclose($handle);
    }
    return $row;
}
function addUser($id, $mdp)
{
   if(($handle=fopen('csv/user.csv.txt','a')) !== FALSE)
    {
        $ligne=array($id,$mdp);
        fputcsv($handle, $ligne, ';');
    }
    fclose($handle);
}
function getPostById($id)
{
    $row=1;
    if(($handle=fopen('csv/post.csv.txt','r')) !== FALSE)
    {
        while(($data=fgetcsv($handle,1000,';')) !==FALSE)
        {
            if($id==$data[0]) {
                $post=$data;
                break;
            }
            $row++;
        }
        fclose($handle);
    }
    return $post;
}
function getAllPosts()
{
    $list=array();
    $row=1;
    if(($handle=fopen('csv/post.csv.txt','r')) !== FALSE)
    {
        while(($data=fgetcsv($handle,1000,';')) !==FALSE)
        {
            if($row == 1){ $row++; continue; }
            array_push($list,$data);
            $row++;
        }
        fclose($handle);
    }
    return $list;
}
function getAllBlogsById($id)
{
    $list=array();
    $row=1;
    if(($handle=fopen('csv/blog.csv.txt','r')) !== FALSE)
    {
        while(($data=fgetcsv($handle,1000,';')) !==FALSE)
        {
            if($data[2]==$id)
            {
                array_push($list,$data);
            }
            $row++;
        }
        fclose($handle);
    }
    return $list;
}
function getAllPostsByIds($idu,$idb)
{
    $list=array();
    $row=1;
    if(($handle=fopen('csv/post.csv.txt','r')) !== FALSE)
    {
        while(($data=fgetcsv($handle,1000,';')) !==FALSE)
        {
            if($data[1]==$idu && $data[5]==$idb)
            {
                array_push($list,$data);
            }
            $row++;
        }
        fclose($handle);
    }
    return $list;
}
function getAllPostsById($id)
{
    $list=array();
    $row=1;
    if(($handle=fopen('csv/post.csv.txt','r')) !== FALSE)
    {
        while(($data=fgetcsv($handle,1000,';')) !==FALSE)
        {
            if($row == 1){ $row++; continue; }
            if($data[1]==$id)
            {
                array_push($list,$data);
            }
            $row++;
        }
        fclose($handle);
    }
    return $list;
}
function addBlog($user,$title,$date){
    $fp=fopen('csv/blog.csv.txt','a');
    $nb=getNbBlogs()+1;
    $ligne=array($nb,$title, $user,$date);
    fputcsv($fp, $ligne, ';');
    echo'Blog créé !';
}
function addPost($username,$title,$date,$content,$id)
{
    $fp=fopen('csv/post.csv.txt','a');
    $nb=getNbPosts()+1;
    $ligne=array($nb, $username,$title,$content,$date,$id);
    fputcsv($fp, $ligne, ';');
    echo "<script>Votre post a bien �t� ajout�.</script>";
    header("Location: list.php?id=".$id);
}
function getNbBlogs()
{
    $row=0;
    if(($handle=fopen('csv/blog.csv.txt','r')) !== FALSE)
    {
        while(($data=fgetcsv($handle,1000,';')) !==FALSE)
        {
            $row++;
        }
        fclose($handle);
    }
    $row=$row-1;
    return $row;
}
function getNbBlogsById($id)
{
	$row=0;
	if(($handle=fopen('csv/blog.csv.txt','r')) !== FALSE)
	{
		while(($data=fgetcsv($handle,1000,';')) !==FALSE)
		{
			if($data[2]==$id)
			{$row++;}
		}
		fclose($handle);
	}
	return $row;
}
function getNbPosts()
{
    $row=0;
    if(($handle=fopen('csv/post.csv.txt','r')) !== FALSE)
    {
        while(($data=fgetcsv($handle,1000,';')) !==FALSE)
        {
            $row++;
        }
        fclose($handle);
    }
    $row=$row-1;
    return $row;
}
function getNbPostsByIds($idu,$idb)
{
    $row=0;
    if(($handle=fopen('csv/post.csv.txt','r')) !== FALSE)
    {
        while(($data=fgetcsv($handle,1000,';')) !==FALSE)
        {
            if($data[1]==$idu && $data[5]==$idb)
            {
                $row++;
            }
        }
        fclose($handle);
    }

    return $row;
}
function getNbPostsById($id)
{
    $row=0;
    if(($handle=fopen('csv/post.csv.txt','r')) !== FALSE)
    {
        while(($data=fgetcsv($handle,1000,';')) !==FALSE)
        {
            if($data[1]==$id)
            {
                $row++;
            }

        }
        fclose($handle);
    }

    return $row;
}
function verifyUser($id, $pswd) //Verify ids
{
    $user=getUser($id);
    if($user!=null)
    {
        if($pswd == $user["pswd"])
        {
        	header('Location: index.php');
        	 
            $_SESSION['username'] = $id;
        }
        else
        {
            echo 'Mauvais mdp.';
        }
    }
    else{
        echo'Mauvais identifiant.';
    }
}
function getUser($id) //Get all informations about the user with id $id
{
	$user=null;
    $row=1;
    if(($handle=fopen('csv/user.csv.txt','r')) !== FALSE)
    {
        while(($data=fgetcsv($handle,1000,';')) !==FALSE)
        {
            if($row!=1)
            {
                if($data[0]==$id)
                {
                    $user=array(
                        "username"=>$id,
                        "pswd"=>$data[1],
                    );
                    break;
                }
            }
            $row++;
        }
        fclose($handle);
    }
    else{
        echo 'erreur de chargement du fichier';
    }
    return $user;
}